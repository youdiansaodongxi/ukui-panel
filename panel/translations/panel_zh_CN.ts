<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>UkuiPanel::Panel</name>
    <message>
        <source>Top</source>
        <translation>上</translation>
    </message>
    <message>
        <source>Bottom</source>
        <translation>下</translation>
    </message>
    <message>
        <source>Left</source>
        <translation>左</translation>
    </message>
    <message>
        <source>Right</source>
        <translation>右</translation>
    </message>
    <message>
        <source>Large</source>
        <translation>大尺寸</translation>
    </message>
    <message>
        <source>Medium</source>
        <translation>中尺寸</translation>
    </message>
    <message>
        <source>Small</source>
        <translation>小尺寸</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation>自定义</translation>
    </message>
    <message>
        <source>Panel Position</source>
        <translation>任务栏位置</translation>
    </message>
    <message>
        <source>Panel Size</source>
        <translation>任务栏尺寸</translation>
    </message>
    <message>
        <source>Lock Panel</source>
        <translation>锁定任务栏</translation>
    </message>
    <message>
        <source>Auto Hide</source>
        <translation>自动隐藏任务栏</translation>
    </message>
    <message>
        <source>System Monitor</source>
        <translation>系统监视器</translation>
    </message>
    <message>
        <source>Show Desktop</source>
        <translation>显示桌面</translation>
    </message>
    <message>
        <source>Panel Setting</source>
        <translation>任务栏设置</translation>
    </message>
    <message>
        <source>Show</source>
        <translation>显示</translation>
    </message>
    <message>
        <source>Switch to New Panel</source>
        <translation>切换三岛任务栏</translation>
    </message>
</context>
<context>
    <name>UkuiPanel::PanelNext</name>
    <message>
        <source>Panel Settings</source>
        <translation>任务栏设置</translation>
    </message>
    <message>
        <source>Show Desktop</source>
        <translation>显示桌面</translation>
    </message>
    <message>
        <source>System Monitor</source>
        <translation>系统监视器</translation>
    </message>
    <message>
        <source>Data Island Form</source>
        <translation>数据岛形式</translation>
    </message>
    <message>
        <source>Islands</source>
        <translation>三岛</translation>
    </message>
    <message>
        <source>Top Bar</source>
        <translation>顶栏</translation>
    </message>
    <message>
        <source>Settings Island Form</source>
        <translation>配置岛形式</translation>
    </message>
    <message>
        <source>Auto Hide</source>
        <translation>自动隐藏任务栏</translation>
    </message>
    <message>
        <source>Lock Panel</source>
        <translation>锁定任务栏</translation>
    </message>
    <message>
        <source>Switch to Classical Panel</source>
        <translation>切换经典任务栏</translation>
    </message>
    <message>
        <source>Panel Size</source>
        <translation>任务栏尺寸</translation>
    </message>
    <message>
        <source>Large</source>
        <translation>大尺寸</translation>
    </message>
    <message>
        <source>Medium</source>
        <translation>中尺寸</translation>
    </message>
    <message>
        <source>Small</source>
        <translation>小尺寸</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation>自定义</translation>
    </message>
</context>
<context>
    <name>UkuiPanel::PanelTopBar</name>
    <message>
        <source>Panel Settings</source>
        <translation>任务栏设置</translation>
    </message>
    <message>
        <source>Show Desktop</source>
        <translation>显示桌面</translation>
    </message>
    <message>
        <source>System Monitor</source>
        <translation>系统监视器</translation>
    </message>
    <message>
        <source>Data Island Form</source>
        <translation>数据岛形式</translation>
    </message>
    <message>
        <source>Islands</source>
        <translation>三岛</translation>
    </message>
    <message>
        <source>Top Bar</source>
        <translation>顶栏</translation>
    </message>
    <message>
        <source>Settings Island Form</source>
        <translation>配置岛形式</translation>
    </message>
    <message>
        <source>Switch to Classical Panel</source>
        <translation>切换经典任务栏</translation>
    </message>
</context>
</TS>
