<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk">
<context>
    <name>UkuiPanel::Panel</name>
    <message>
        <source>Show Desktop</source>
        <translation>ۇستەل بەتىن كورسەتۋ</translation>
    </message>
    <message>
        <source>System Monitor</source>
        <translation>سەستيما كۇزەتۋشى</translation>
    </message>
    <message>
        <source>Auto Hide</source>
        <translation>اۆتوماتتى جاسىرۋ</translation>
    </message>
    <message>
        <source>Panel Setting</source>
        <translation>تاقتا تەڭگەرگٸش</translation>
    </message>
    <message>
        <source>Panel Size</source>
        <translation>مىندەتتى ستوننىڭ ۇلكەن-كشىلگى</translation>
    </message>
    <message>
        <source>Large</source>
        <translation>ۇلكەن</translation>
    </message>
    <message>
        <source>Medium</source>
        <translation>ورتا</translation>
    </message>
    <message>
        <source>Small</source>
        <translation>كشكەنە</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation>عۇرىپ-ادەت</translation>
    </message>
    <message>
        <source>Panel Position</source>
        <translation>تاقتا ورنى</translation>
    </message>
    <message>
        <source>Top</source>
        <translation>ەڭ ٷستٸن</translation>
    </message>
    <message>
        <source>Bottom</source>
        <translation>استڭقٸ</translation>
    </message>
    <message>
        <source>Left</source>
        <translation>سول</translation>
    </message>
    <message>
        <source>Right</source>
        <translation>تۋراسى</translation>
    </message>
    <message>
        <source>Lock Panel</source>
        <translation>مىندەتتى ستونى قۇلپىلاۋ</translation>
    </message>
    <message>
        <source>Show</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Switch to New Panel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UkuiPanel::PanelNext</name>
    <message>
        <source>Panel Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Desktop</source>
        <translation type="unfinished">ۇستەل بەتىن كورسەتۋ</translation>
    </message>
    <message>
        <source>System Monitor</source>
        <translation type="unfinished">سەستيما كۇزەتۋشى</translation>
    </message>
    <message>
        <source>Data Island Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Islands</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Top Bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Settings Island Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Auto Hide</source>
        <translation type="unfinished">اۆتوماتتى جاسىرۋ</translation>
    </message>
    <message>
        <source>Lock Panel</source>
        <translation type="unfinished">مىندەتتى ستونى قۇلپىلاۋ</translation>
    </message>
    <message>
        <source>Switch to Classical Panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Panel Size</source>
        <translation type="unfinished">مىندەتتى ستوننىڭ ۇلكەن-كشىلگى</translation>
    </message>
    <message>
        <source>Large</source>
        <translation type="unfinished">ۇلكەن</translation>
    </message>
    <message>
        <source>Medium</source>
        <translation type="unfinished">ورتا</translation>
    </message>
    <message>
        <source>Small</source>
        <translation type="unfinished">كشكەنە</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation type="unfinished">عۇرىپ-ادەت</translation>
    </message>
</context>
<context>
    <name>UkuiPanel::PanelTopBar</name>
    <message>
        <source>Panel Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Desktop</source>
        <translation type="unfinished">ۇستەل بەتىن كورسەتۋ</translation>
    </message>
    <message>
        <source>System Monitor</source>
        <translation type="unfinished">سەستيما كۇزەتۋشى</translation>
    </message>
    <message>
        <source>Data Island Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Islands</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Top Bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Settings Island Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Switch to Classical Panel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
