<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>UkuiPanel::Panel</name>
    <message>
        <source>Show Desktop</source>
        <translation>ཅོག་ངོས་འཆར་བ།</translation>
    </message>
    <message>
        <source>System Monitor</source>
        <translation>མ་ལག་གི་ལྟ་ཞིབ་ཡོ་བྱད།</translation>
    </message>
    <message>
        <source>Auto Hide</source>
        <translation>རང་འགུལ་གྱིས་ལས་འགན་སྦས་སྐུང་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Panel Setting</source>
        <translation>ལས་འགན་གྱི་བསྒར་བྱང་འཛུགས་པ།</translation>
    </message>
    <message>
        <source>Panel Size</source>
        <translation>ལས་འགན་གྱི་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <source>Large</source>
        <translation>ཆེ་བ།</translation>
    </message>
    <message>
        <source>Medium</source>
        <translation>འབྲིང་བ།</translation>
    </message>
    <message>
        <source>Small</source>
        <translation>ཆུང་བ།</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation>རང་དོན།</translation>
    </message>
    <message>
        <source>Panel Position</source>
        <translation>ལས་འགན་གྱི་གནས་ས།</translation>
    </message>
    <message>
        <source>Top</source>
        <translation>སྟོད་ཆ།</translation>
    </message>
    <message>
        <source>Bottom</source>
        <translation>ཞབས་ཁུལ།</translation>
    </message>
    <message>
        <source>Left</source>
        <translation>གཡོན་ཕྱོགས་པ།</translation>
    </message>
    <message>
        <source>Right</source>
        <translation>གཡས་ཕྱོགས་པ།</translation>
    </message>
    <message>
        <source>Lock Panel</source>
        <translation>ལས་འགན་གྱི་ལན་ཀན་བཀག་སྡོམ་བྱེད་</translation>
    </message>
    <message>
        <source>Show</source>
        <translation>གསལ་པོར་མངོན་པ།</translation>
    </message>
    <message>
        <source>Switch to New Panel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UkuiPanel::PanelNext</name>
    <message>
        <source>Panel Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Desktop</source>
        <translation type="unfinished">ཅོག་ངོས་འཆར་བ།</translation>
    </message>
    <message>
        <source>System Monitor</source>
        <translation type="unfinished">མ་ལག་གི་ལྟ་ཞིབ་ཡོ་བྱད།</translation>
    </message>
    <message>
        <source>Data Island Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Islands</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Top Bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Settings Island Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Auto Hide</source>
        <translation type="unfinished">རང་འགུལ་གྱིས་ལས་འགན་སྦས་སྐུང་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Lock Panel</source>
        <translation type="unfinished">ལས་འགན་གྱི་ལན་ཀན་བཀག་སྡོམ་བྱེད་</translation>
    </message>
    <message>
        <source>Switch to Classical Panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Panel Size</source>
        <translation type="unfinished">ལས་འགན་གྱི་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <source>Large</source>
        <translation type="unfinished">ཆེ་བ།</translation>
    </message>
    <message>
        <source>Medium</source>
        <translation type="unfinished">འབྲིང་བ།</translation>
    </message>
    <message>
        <source>Small</source>
        <translation type="unfinished">ཆུང་བ།</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation type="unfinished">རང་དོན།</translation>
    </message>
</context>
<context>
    <name>UkuiPanel::PanelTopBar</name>
    <message>
        <source>Panel Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Desktop</source>
        <translation type="unfinished">ཅོག་ངོས་འཆར་བ།</translation>
    </message>
    <message>
        <source>System Monitor</source>
        <translation type="unfinished">མ་ལག་གི་ལྟ་ཞིབ་ཡོ་བྱད།</translation>
    </message>
    <message>
        <source>Data Island Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Islands</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Top Bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Settings Island Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Switch to Classical Panel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
