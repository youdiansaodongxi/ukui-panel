<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>UkuiPanel::Panel</name>
    <message>
        <source>Show Desktop</source>
        <translation>Afficher le bureau</translation>
    </message>
    <message>
        <source>System Monitor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Auto Hide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Panel Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Panel Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Large</source>
        <translation>Grand</translation>
    </message>
    <message>
        <source>Medium</source>
        <translation>Douleur moyenne</translation>
    </message>
    <message>
        <source>Small</source>
        <translation>Petit</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation>Coutume</translation>
    </message>
    <message>
        <source>Panel Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Top</source>
        <translation>Retour au début</translation>
    </message>
    <message>
        <source>Bottom</source>
        <translation>Fond</translation>
    </message>
    <message>
        <source>Left</source>
        <translation>Gauche</translation>
    </message>
    <message>
        <source>Right</source>
        <translation>Droite</translation>
    </message>
    <message>
        <source>Lock Panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Switch to New Panel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UkuiPanel::PanelNext</name>
    <message>
        <source>Panel Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Desktop</source>
        <translation type="unfinished">Afficher le bureau</translation>
    </message>
    <message>
        <source>System Monitor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Data Island Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Islands</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Top Bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Settings Island Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Auto Hide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Lock Panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Switch to Classical Panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Panel Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Large</source>
        <translation type="unfinished">Grand</translation>
    </message>
    <message>
        <source>Medium</source>
        <translation type="unfinished">Douleur moyenne</translation>
    </message>
    <message>
        <source>Small</source>
        <translation type="unfinished">Petit</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation type="unfinished">Coutume</translation>
    </message>
</context>
<context>
    <name>UkuiPanel::PanelTopBar</name>
    <message>
        <source>Panel Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Desktop</source>
        <translation type="unfinished">Afficher le bureau</translation>
    </message>
    <message>
        <source>System Monitor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Data Island Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Islands</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Top Bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Settings Island Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Switch to Classical Panel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
