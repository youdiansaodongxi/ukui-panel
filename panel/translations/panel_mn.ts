<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>UkuiPanel::Panel</name>
    <message>
        <source>Show Desktop</source>
        <translation>ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠢ᠋ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <source>System Monitor</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮᠲᠦ ᠬᠢᠨᠠᠨ ᠠᠵᠢᠭᠯᠠᠬᠤ ᠮᠠᠰᠢᠨ</translation>
    </message>
    <message>
        <source>Auto Hide</source>
        <translation>ᠠᠦᠢᠲ᠋ᠣ᠋ ᠨᠢᠭᠤᠨ ᠬᠠᠳᠠᠭᠠᠯᠠ</translation>
    </message>
    <message>
        <source>Panel Setting</source>
        <translation>ᠡᠭᠦᠷᠭᠡ ᠶᠢᠨ ᠪᠤᠯᠤᠩ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Panel Size</source>
        <translation>ᠬᠠᠪᠲᠠᠰᠤ ᠶᠢᠨ ᠨᠤᠮᠤ</translation>
    </message>
    <message>
        <source>Large</source>
        <translation>ᠶᠡᠬᠡ</translation>
    </message>
    <message>
        <source>Medium</source>
        <translation>ᠳᠤᠮᠳᠠ</translation>
    </message>
    <message>
        <source>Small</source>
        <translation>ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation>ᠦᠪᠡᠷᠳᠡᠬᠡᠨ ᠳᠤᠭᠳᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Panel Position</source>
        <translation>ᠡᠭᠦᠷᠭᠡ ᠶᠢᠨ ᠪᠤᠯᠤᠩ ᠤᠨ ᠪᠠᠶᠢᠷᠢ</translation>
    </message>
    <message>
        <source>Top</source>
        <translation>ᠣᠷᠣᠢ᠎ᠶ᠋ᠢᠨ ᠬᠡᠰᠡᠭ</translation>
    </message>
    <message>
        <source>Bottom</source>
        <translation>ᠰᠠᠭᠤᠷᠢ</translation>
    </message>
    <message>
        <source>Left</source>
        <translation>ᠵᠡᠬᠦᠨ</translation>
    </message>
    <message>
        <source>Right</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨ</translation>
    </message>
    <message>
        <source>Lock Panel</source>
        <translation>ᠣᠨᠢᠰᠤᠨ ᠬᠠᠪᠲᠠᠰᠤ</translation>
    </message>
    <message>
        <source>Show</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Switch to New Panel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UkuiPanel::PanelNext</name>
    <message>
        <source>Panel Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Desktop</source>
        <translation type="unfinished">ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠢ᠋ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <source>System Monitor</source>
        <translation type="unfinished">ᠰᠢᠰᠲ᠋ᠧᠮᠲᠦ ᠬᠢᠨᠠᠨ ᠠᠵᠢᠭᠯᠠᠬᠤ ᠮᠠᠰᠢᠨ</translation>
    </message>
    <message>
        <source>Data Island Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Islands</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Top Bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Settings Island Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Auto Hide</source>
        <translation type="unfinished">ᠠᠦᠢᠲ᠋ᠣ᠋ ᠨᠢᠭᠤᠨ ᠬᠠᠳᠠᠭᠠᠯᠠ</translation>
    </message>
    <message>
        <source>Lock Panel</source>
        <translation type="unfinished">ᠣᠨᠢᠰᠤᠨ ᠬᠠᠪᠲᠠᠰᠤ</translation>
    </message>
    <message>
        <source>Switch to Classical Panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Panel Size</source>
        <translation type="unfinished">ᠬᠠᠪᠲᠠᠰᠤ ᠶᠢᠨ ᠨᠤᠮᠤ</translation>
    </message>
    <message>
        <source>Large</source>
        <translation type="unfinished">ᠶᠡᠬᠡ</translation>
    </message>
    <message>
        <source>Medium</source>
        <translation type="unfinished">ᠳᠤᠮᠳᠠ</translation>
    </message>
    <message>
        <source>Small</source>
        <translation type="unfinished">ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation type="unfinished">ᠦᠪᠡᠷᠳᠡᠬᠡᠨ ᠳᠤᠭᠳᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>UkuiPanel::PanelTopBar</name>
    <message>
        <source>Panel Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Desktop</source>
        <translation type="unfinished">ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠢ᠋ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <source>System Monitor</source>
        <translation type="unfinished">ᠰᠢᠰᠲ᠋ᠧᠮᠲᠦ ᠬᠢᠨᠠᠨ ᠠᠵᠢᠭᠯᠠᠬᠤ ᠮᠠᠰᠢᠨ</translation>
    </message>
    <message>
        <source>Data Island Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Islands</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Top Bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Settings Island Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Switch to Classical Panel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
