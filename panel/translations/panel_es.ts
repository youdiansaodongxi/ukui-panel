<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es">
<context>
    <name>UkuiPanel::Panel</name>
    <message>
        <source>Show Desktop</source>
        <translation>Mostrar escritorio</translation>
    </message>
    <message>
        <source>System Monitor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Auto Hide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Panel Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Panel Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Large</source>
        <translation>Grande</translation>
    </message>
    <message>
        <source>Medium</source>
        <translation>Medio</translation>
    </message>
    <message>
        <source>Small</source>
        <translation>Pequeño</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation>Costumbre</translation>
    </message>
    <message>
        <source>Panel Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Top</source>
        <translation>Arriba</translation>
    </message>
    <message>
        <source>Bottom</source>
        <translation>Fondo</translation>
    </message>
    <message>
        <source>Left</source>
        <translation>Izquierda</translation>
    </message>
    <message>
        <source>Right</source>
        <translation>Derecha</translation>
    </message>
    <message>
        <source>Lock Panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Switch to New Panel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UkuiPanel::PanelNext</name>
    <message>
        <source>Panel Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Desktop</source>
        <translation type="unfinished">Mostrar escritorio</translation>
    </message>
    <message>
        <source>System Monitor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Data Island Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Islands</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Top Bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Settings Island Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Auto Hide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Lock Panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Switch to Classical Panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Panel Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Large</source>
        <translation type="unfinished">Grande</translation>
    </message>
    <message>
        <source>Medium</source>
        <translation type="unfinished">Medio</translation>
    </message>
    <message>
        <source>Small</source>
        <translation type="unfinished">Pequeño</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation type="unfinished">Costumbre</translation>
    </message>
</context>
<context>
    <name>UkuiPanel::PanelTopBar</name>
    <message>
        <source>Panel Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Desktop</source>
        <translation type="unfinished">Mostrar escritorio</translation>
    </message>
    <message>
        <source>System Monitor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Data Island Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Islands</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Top Bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Settings Island Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Switch to Classical Panel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
