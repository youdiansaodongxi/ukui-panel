<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>PanelUkccPlugin</name>
    <message>
        <source>Panel</source>
        <translation>ᠬᠠᠪᠲᠠᠰᠤ</translation>
        <extra-contents_path>/Panel/Panel</extra-contents_path>
    </message>
    <message>
        <source>Merge icons on the taskbar</source>
        <translation>ᠡᠭᠦᠷᠭᠡ ᠶᠢᠨ ᠪᠤᠯᠤᠩ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠨᠡᠶᠢᠯᠡᠭᠦᠯᠬᠦ ᠵᠢᠷᠤᠭ</translation>
        <extra-contents_path>/Panel/Merge icons on the taskbar</extra-contents_path>
    </message>
    <message>
        <source>Always</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Never</source>
        <translation>ᠶᠡᠷᠦ ᠡᠴᠡ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Panel location</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Panel/Panel location</extra-contents_path>
    </message>
    <message>
        <source>Bottom</source>
        <translation>ᠰᠠᠭᠤᠷᠢ</translation>
    </message>
    <message>
        <source>Left</source>
        <translation>ᠵᠡᠬᠦᠨ</translation>
    </message>
    <message>
        <source>Top</source>
        <translation>ᠣᠷᠣᠢ᠎ᠶ᠋ᠢᠨ ᠬᠡᠰᠡᠭ</translation>
    </message>
    <message>
        <source>Right</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨ</translation>
    </message>
    <message>
        <source>Panel size</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Panel/Panel size</extra-contents_path>
    </message>
    <message>
        <source>Small</source>
        <translation>ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <source>Medium</source>
        <translation>ᠳᠤᠮᠳᠠ</translation>
    </message>
    <message>
        <source>Large</source>
        <translation>ᠶᠡᠬᠡ</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation>ᠦᠪᠡᠷᠳᠡᠬᠡᠨ ᠳᠤᠭᠳᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Panel auto hide</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Panel/Panel auto hide</extra-contents_path>
    </message>
    <message>
        <source>Panel lock</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Panel/Panel lock</extra-contents_path>
    </message>
    <message>
        <source>Widgets always showed on panel</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Panel/Widgets always showed on panel</extra-contents_path>
    </message>
    <message>
        <source>Task view</source>
        <translation type="unfinished">多任务视图</translation>
    </message>
    <message>
        <source>Volume</source>
        <translation>ᠳᠠᠭᠤᠨ ᠤ᠋ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <source>Network</source>
        <translation>ᠰᠦᠯᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <source>Power</source>
        <translation>ᠴᠠᠬᠢᠯᠭᠠᠨ ᠡᠭᠦᠰᠭᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <source>Bluetooth</source>
        <translation>ᠬᠥᠬᠡ ᠰᠢᠳᠦ</translation>
    </message>
    <message>
        <source>Icons showed on system tray</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Panel/Icons showed on system tray</extra-contents_path>
    </message>
    <message>
        <source>Panel Appearance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>classic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>threeIsland</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show panel on all screens</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Panel/Show panel on all screens</extra-contents_path>
    </message>
    <message>
        <source>Show system tray area on all panels </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>When existing multiple panels, show window icons on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>All panels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Primary screen panel and panel where window is open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Panel where window is open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Virtual Keyboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>VPN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fcitx</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SwitchButton</name>
    <message>
        <source>Task view</source>
        <translation type="obsolete">多任务视图</translation>
    </message>
</context>
</TS>
