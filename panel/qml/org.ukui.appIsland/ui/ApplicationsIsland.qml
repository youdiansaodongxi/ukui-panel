/*
 * Copyright (C) 2024, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: iaom <zhangpengfei@kylinos.cn>
 *
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15

import org.ukui.quick.widgets 1.0
import org.ukui.quick.container 1.0
import org.ukui.quick.items 1.0
import org.ukui.quick.platform 1.0
import org.ukui.panel.impl 1.0 as Panel

WidgetContainerItem {
    id: containerItem
    property int orientation: WidgetContainer.orientation
    property bool isHorizontal: orientation === Types.Horizontal
    property bool fillWidth: false
    property double ratio: parent.ratio ? parent.ratio : 1
    Layout.preferredWidth: backGround.width
    Layout.maximumWidth: parent.Layout.maximumWidth

    Component.onCompleted: {
        fillWidth = Qt.binding(function (){
            for (var i = 0; i < repeater.count; i++) {
                if(repeater.itemAt(i).Layout.fillWidth) {
                    return true;
                }
            }
            return false
        });
    }

    StyleBackground {
        id: backGround
        property double ratio: containerItem.ratio
        Binding on width {
            value: {
                if (containerItem.fillWidth) {
                    return containerItem.width
                } else {
                    return mainLayout.childrenRect.width + mainLayout.anchors.leftMargin + mainLayout.anchors.rightMargin
                }
            }
            delayed: true;
        }
        height: parent.height
        borderColor: Theme.ButtonText
        borderAlpha: 0.25
        border.width: 1
        radius: ratio * Theme.windowRadius

        GridLayout {
            id: mainLayout
            z: 10
            anchors.fill: parent
            anchors {
                leftMargin: 12 * backGround.ratio
                topMargin: 0
                rightMargin: 12 * backGround.ratio
                bottomMargin: 0
            }
            rows: containerItem.isHorizontal ? 1 : repeater.count
            columns: containerItem.isHorizontal ? repeater.count : 1
            rowSpacing: 4 * backGround.ratio
            columnSpacing: 4 * backGround.ratio
            property var otherPluginsWidth: {"": 0}

            function otherPluginsTotalWidth() {
                let sum = 0;

                for (var key in mainLayout.otherPluginsWidth) {
                    var width = mainLayout.otherPluginsWidth[key];
                    if (typeof width === 'number') {
                        sum += width;
                    }
                }
                return sum;
            }
            Repeater {
                id: repeater
                model: Panel.WidgetSortModel {
                    // TODO: use config
                    widgetOrder: containerItem.WidgetContainer.config.widgetsOrder
                    sourceModel: containerItem.widgetItemModel
                }
                delegate: widgetLoaderComponent
            }
        }

        Component {
            id: widgetLoaderComponent

            Item {
                id: widgetParent
                clip: true

                property int index: model.index
                property double ratio: backGround.ratio
                property Item widgetItem: model.widgetItem
                property string itemId: widgetItem.Widget.id
                property bool isIslandMode: true

                Layout.fillWidth: widgetItem.Layout.fillWidth
                Layout.fillHeight: widgetItem.Layout.fillHeight

                Layout.minimumWidth: widgetItem.Layout.minimumWidth
                Layout.minimumHeight: widgetItem.Layout.minimumHeight

                Layout.maximumWidth: (itemId === "org.ukui.panel.taskManager") ?
                                     (containerItem.Layout.maximumWidth - mainLayout.otherPluginsTotalWidth()
                                     - mainLayout.rowSpacing*repeater.count - mainLayout.anchors.leftMargin - mainLayout.anchors.rightMargin)
                                     : widgetItem.Layout.maximumWidth

                Layout.maximumHeight: widgetItem.Layout.maximumHeight

                Layout.preferredWidth: widgetItem.Layout.preferredWidth
                Layout.preferredHeight: widgetItem.Layout.preferredHeight

                onWidthChanged: {
                    if (itemId !== "org.ukui.panel.taskManager") {
                        mainLayout.otherPluginsWidth[widgetItem.Widget.instanceId] = width;
                    }
                }
                onWidgetItemChanged: {
                    if (widgetItem) {
                        widgetItem.parent = widgetParent
                        widgetItem.anchors.fill = widgetParent
                    }
                }
            }
        }
    }
}
