<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>Calendar</name>
    <message>
        <source> notification</source>
        <translation> བརྡ་ཐོ།</translation>
    </message>
    <message>
        <source> notifications</source>
        <translation> བརྡ་ཐོ།</translation>
    </message>
    <message>
        <source>Notification center</source>
        <translation>ལྟེ་གནས་ལ་བརྡ་ཐོ་གཏོང</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>Time and Date Setting</source>
        <translation>དུས་ཚོད་ཀྱི་ཚེས་གྲངས་བཀོད་སྒྲིག་བྱ།</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>ཁ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <source>Set up notification center</source>
        <translation>བརྡ་ཐོའི་ལྟེ་གནས་འཛུགས་དགོས།</translation>
    </message>
</context>
</TS>
